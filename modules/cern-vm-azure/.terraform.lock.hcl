# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/cernops/cern" {
  version     = "2.1.3"
  constraints = "2.1.3"
  hashes = [
    "h1:YpjlNW9jKaM2nmZg5Hvpg0qid793/2j6Q0o4XMia2ko=",
    "zh:084c3492d7a5355aece5e2402b4d169d45796fc658f3f79355a89b20158e57ad",
    "zh:148bd57c0625f6b9a94f19413dc9adb5be25205eb58d494923e57314e1feabd8",
    "zh:28a390387d6e4fd4ad94ac339efb218e94d223a1a39bf395cbe4215d3f8b8c07",
    "zh:32d0a8ba2a88cc0092daa85cbbcac980fe819feac3be2dd0de8c687d6e53bbe8",
    "zh:401c85e8a1a926dccbf0b07a67b3c71de765ea11e913c768a6c1d7df872d4444",
    "zh:55f6b375989f7ea88cef2d9e8a8ae07b652f34e984f4051198b678e685458e96",
    "zh:6d7b42d5d30f85ff90f0b0498d00b4b7d0bcbfd6f423400b2d873122e65e35f8",
    "zh:7af89cec72b74e198a0e388a7cb0dccf1bb457b87ee6354a2d90d3d30b71d6e3",
    "zh:8474b37afa537728384762a86767d7e2b14b85592c1835c09bc1a380404bd986",
    "zh:893e1b7942b0aed315539e6f68b49457bcf2a8ef7763e7873e2350f6547d2fdc",
    "zh:c2151077ef5ac185717ce56faa8877eb94405308d002a70af37205b7178f4feb",
    "zh:e579200db3879b06e3def688d114979cdc13ead1839be62d94ac6b080da2b4bb",
    "zh:e90cd4af87b196488a090788729b1dc11bbc3229e57096bc579eaaa36b7676a7",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
    "zh:fca8b251f66fb7bbfeff9b1c367f6af3422a83d3535ab209947d99d88df569c3",
  ]
}

provider "registry.terraform.io/hashicorp/azurerm" {
  version     = "2.38.0"
  constraints = "2.38.0"
  hashes = [
    "h1:r7n5rWED/Wlrx7ERKltAnQgEkf7jflRwzVWEuoPbjzg=",
    "zh:0e4ffe01581c53d17c0f3a34f9f30b399babccaf5808df8c4de070b83117c4e7",
    "zh:1b74a8266254348195eda3e90617c0f8ebc1337cb23afae61472806134757fbf",
    "zh:286b34f2002bdda1d576945ce51dbe759a3f9e2dcbaee605f2c8f7193abf9b3e",
    "zh:2fc11de71542ec2cd69138b9de8dce37ae0426942b459d9b39fe89dae8d46d5f",
    "zh:3f9841f91c35e61b02ac486e25da454f3c9bf812fdfff1993b405854aba84c1e",
    "zh:4dd8dd193020dc7894599b7f196371d4143d56da2b0eecf8d96251267719cc92",
    "zh:6ad5e7cf59ae3b95acb4cf2894788cfe9c2573d8c9b703919a3570d09f01bab4",
    "zh:838aa06c138f333c2b1fdb1a5b66b2aa1b397046edfb362ed4c364e5e782623b",
    "zh:bb5c47acd6afd9cc40fbb37c0e7c36b1d597f5f10f7246392f0707244d299ad1",
    "zh:ca4f057ad1b8fbce99e883a7d8bc396fed39c760cab05bf352504effd1dcb97f",
  ]
}

provider "registry.terraform.io/hashicorp/local" {
  version     = "2.0.0"
  constraints = "2.0.0"
  hashes = [
    "h1:EC6eh7avwx1rF56h3RZcxgEp/14ihi7Sk/4J3Hn4nIE=",
    "zh:34ce8b79493ace8333d094752b579ccc907fa9392a2c1d6933a6c95d0786d3f1",
    "zh:5c5a19c4f614a4ffb68bae0b0563f3860115cf7539b8adc21108324cfdc10092",
    "zh:67ddb1ca2cd3e1a8f948302597ceb967f19d2eeb2d125303493667388fe6330e",
    "zh:68e6b16f3a8e180fcba1a99754118deb2d82331b51f6cca39f04518339bfdfa6",
    "zh:8393a12eb11598b2799d51c9b0a922a3d9fadda5a626b94a1b4914086d53120e",
    "zh:90daea4b2010a86f2aca1e3a9590e0b3ddcab229c2bd3685fae76a832e9e836f",
    "zh:99308edc734a0ac9149b44f8e316ca879b2670a1cae387a8ae754c180b57cdb4",
    "zh:c76594db07a9d1a73372a073888b672df64adb455d483c2426cc220eda7e092e",
    "zh:dc09c1fb36c6a706bdac96cce338952888c8423978426a09f5df93031aa88b84",
    "zh:deda88134e9780319e8de91b3745520be48ead6ec38cb662694d09185c3dac70",
  ]
}

provider "registry.terraform.io/hashicorp/null" {
  version = "3.1.1"
  hashes = [
    "h1:71sNUDvmiJcijsvfXpiLCz0lXIBSsEJjMxljt7hxMhw=",
    "zh:063466f41f1d9fd0dd93722840c1314f046d8760b1812fa67c34de0afcba5597",
    "zh:08c058e367de6debdad35fc24d97131c7cf75103baec8279aba3506a08b53faf",
    "zh:73ce6dff935150d6ddc6ac4a10071e02647d10175c173cfe5dca81f3d13d8afe",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:8fdd792a626413502e68c195f2097352bdc6a0df694f7df350ed784741eb587e",
    "zh:976bbaf268cb497400fd5b3c774d218f3933271864345f18deebe4dcbfcd6afa",
    "zh:b21b78ca581f98f4cdb7a366b03ae9db23a73dfa7df12c533d7c19b68e9e72e5",
    "zh:b7fc0c1615dbdb1d6fd4abb9c7dc7da286631f7ca2299fb9cd4664258ccfbff4",
    "zh:d1efc942b2c44345e0c29bc976594cb7278c38cfb8897b344669eafbc3cddf46",
    "zh:e356c245b3cd9d4789bab010893566acace682d7db877e52d40fc4ca34a50924",
    "zh:ea98802ba92fcfa8cf12cbce2e9e7ebe999afbf8ed47fa45fc847a098d89468b",
    "zh:eff8872458806499889f6927b5d954560f3d74bf20b6043409edf94d26cd906f",
  ]
}
