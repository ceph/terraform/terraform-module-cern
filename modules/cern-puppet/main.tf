resource "random_string" "suffix" {
  count   = var.suffix_enabled ? 1 : 0
  length  = 10
  special = false

  lifecycle {
    ignore_changes = [
      special,
      length
    ]
  }
}

locals {
  name = "${local.name_sanitized}${var.suffix_enabled ? random_string.suffix[0].result : ""}"
}

data "foreman_hostgroup" "hg" {
  title = var.hostgroup
}
data "foreman_environment" "env" {
  count = var.environment == null ? 0 : 1
  name  = var.environment
}

data "openstack_compute_flavor_v2" "flavor" {
  flavor_id = var.flavor_id
  name      = var.flavor_name
}

locals {
  is_physical_flavor  = tobool(lookup(data.openstack_compute_flavor_v2.flavor.extra_specs, "cern:physical", false))
  top_level_hostgroup = split("/", var.hostgroup)[0]
}

data "foreman_usergroup" "owner" {
  count = var.owner_type == "Usergroup" ? 1 : 0
  name  = var.owner
}
data "foreman_user" "owner" {
  count = var.owner_type == "User" ? 1 : 0
  login = var.owner
}

resource "foreman_host" "host" {
  name               = "${local.name}.cern.ch"
  hostgroup_id       = data.foreman_hostgroup.hg.id
  environment_id     = var.environment == null ? var.environment_id : data.foreman_environment.env[0].id
  build              = false
  managed            = false
  owner_id           = var.owner_type == "User" ? data.foreman_user.owner[0].id : data.foreman_usergroup.owner[0].id
  owner_type         = var.owner_type
  medium_id          = local.medium_id
  operatingsystem_id = local.operatingsystem_id

  lifecycle {
    ignore_changes = [
      operatingsystem_id,
    ]
  }
}

resource "cern_roger" "state" {
  hostname    = "${local.name}.cern.ch"
  app_alarmed = var.alarms_enabled_roger
  hw_alarmed  = var.alarms_enabled_roger
  nc_alarmed  = var.alarms_enabled_roger
  os_alarmed  = var.alarms_enabled_roger
  appstate    = var.appstate_roger
  count       = var.manage_roger ? 1 : 0
}

data "template_cloudinit_config" "config" {
  gzip          = true
  base64_encode = true

  part {
    content_type = "text/x-shellscript"
    filename     = "puppetinit"
    content = templatefile("${path.module}/userdata.tftpl",
      {
        config = {
          foreman_environment = var.environment
          top_level_hostgroup = local.top_level_hostgroup
        },
        script = "${file("${path.module}/userdata.sh")}"
    })
  }

  dynamic "part" {
    for_each = local.is_physical_flavor ? [1] : []
    # Mount config drive if node is physical
    content {
      content_type = "text/cloud-config"
      content = yamlencode({
        mounts = [
          "/dev/disk/by-label/config-2",
          "/mnt/configdrive"
        ]
      })
    }
  }
}

resource "openstack_compute_instance_v2" "instance" {
  count             = var.manage_instance ? (var.image_legacy ? 0 : 1) : 0
  name              = local.name
  image_id          = var.image_id
  image_name        = var.image_name
  flavor_id         = data.openstack_compute_flavor_v2.flavor.id
  user_data         = data.template_cloudinit_config.config.rendered
  metadata          = local.metadata
  availability_zone = var.availability_zone

  lifecycle {
    ignore_changes = [
      user_data,
    ]
  }

  depends_on = [
    foreman_host.host,
  ]
}

resource "openstack_compute_instance_v2" "instance_legacy" {
  count             = var.manage_instance ? (var.image_legacy ? 1 : 0) : 0
  name              = local.name
  image_id          = var.image_id
  image_name        = var.image_name
  flavor_id         = data.openstack_compute_flavor_v2.flavor.id
  user_data         = data.template_cloudinit_config.config.rendered
  metadata          = local.metadata
  availability_zone = var.availability_zone

  lifecycle {
    ignore_changes = [
      image_id,
      image_name,
      key_pair,
      user_data,
    ]
  }

  depends_on = [
    foreman_host.host,
  ]
}

locals {
  instance = var.manage_instance ? (var.image_legacy ? openstack_compute_instance_v2.instance_legacy[0] : openstack_compute_instance_v2.instance[0]) : null
}

resource "cern_certmgr" "cert" {
  hostname = "${local.name}.cern.ch"
  # TODO: whenever an updated attribute would we available in OpenStack
  #       provider use it as a vm_tag here.
  vm_tag = local.instance != null ? "${local.instance.id}-${local.instance.image_id}" : null
}

