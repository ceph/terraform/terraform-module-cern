output "user_data" {
  value = data.template_cloudinit_config.config.rendered
}
output "foreman_host" {
  value = foreman_host.host
}
output "name_sanitized" {
  value = local.name_sanitized
}
output "instance" {
  value = local.instance
}
output "is_physical_flavor" {
  value = local.is_physical_flavor
}
