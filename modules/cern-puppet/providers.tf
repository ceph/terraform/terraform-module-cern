terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = ">= 1.48.0"
    }
    cern = {
      source  = "cernops/cern"
      version = ">= 2.2.1"
    }
    foreman = {
      source  = "terraform-coop/foreman"
      version = ">= 0.5.0"
    }
    random = {
      source  = "hashicorp/random"
      version = ">= 3.1.3"
    }
  }
}
