variable "name" {
  description = "Name of the node"
  nullable    = false
}
variable "suffix_enabled" {
  description = "Whether or not a random suffix is appended to the node name"
  nullable    = false
  default     = false
}
variable "hostgroup" {
  description = "Name of the foreman hostgroup to use. Either this variable or hostgroup_id has to be set!"
  nullable    = false
}
variable "environment" {
  description = "Name of the foreman environment to use"
  default     = "production"
}
variable "environment_id" {
  description = "Name of the foreman environment to use"
  default     = "production"
}
variable "flavor_id" {
  description = "The id of the OpenStack flavor. Either this variable or flavor has to be set!"
  nullable    = true
  default     = null
}
variable "flavor_name" {
  description = "The name of the OpenStack flavor. Either this variable or flavor_id has to be set!"
  nullable    = true
  default     = null
}
variable "owner" {
  description = "The owner of the node in foreman/landb"
  nullable    = false
}
variable "owner_type" {
  description = "The owner type in foreman, can be either 'User' or 'Usergroup'"
  default     = "Usergroup"
  validation {
    condition     = var.owner_type == "User" || var.owner_type == "Usergroup"
    error_message = "The variable owner_type should be set to 'User' or 'Usergroup'."
  }
}
variable "manage_roger" {
  type        = bool
  description = "Whether or not the module should create the roger state"
  nullable    = false
  default     = true
}
variable "alarms_enabled_roger" {
  type        = bool
  description = "Whether or not all the roger alarms are enabled"
  default     = true
}
variable "appstate_roger" {
  description = "Appstate of roger"
  default     = "production"
}
variable "manage_instance" {
  type        = bool
  description = "Whether or not the module should create the server on OpenStack"
  default     = true
}
variable "image_id" {
  description = "OpenStack image id, only useful if manage_server is enabled"
  nullable    = true
  default     = null
}
variable "image_name" {
  description = "OpenStack image name, only useful if manage_server is enabled"
  nullable    = true
  default     = null
}
variable "metadata" {
  type        = map(any)
  description = "OpenStack metadata, only useful if manage_server is enabled"
  nullable    = false
  default     = {}
}
variable "image_legacy" {
  type        = bool
  description = "Ignore any OpenStack image change. This is useful to import instance created without Terraform"
  default     = false
}
variable "image_centos_stream" {
  type        = bool
  description = "Whether or not the image is a CentOS Stream image. This change the default medium_id and operatingsystem_id for foreman"
  default     = true
}
variable "medium_id" {
  type        = number
  description = "Foreman medium_id, defaults to CentosStream (18) if image_centos_stream or null otherwise"
  nullable    = true
  default     = null
}

variable "operatingsystem_id" {
  type        = number
  description = "Foreman operatingsystem_id, defaults to CentOS Stream 8 (112) if image_centos_stream or null otherwise"
  nullable    = true
  default     = null
}

variable "availability_zone" {
  type        = string
  description = "OpenStack availability zone, only useful if manage_server is enabled"
  nullable    = true
  default     = null
}

locals {
  metadata = merge(var.metadata, {
    landb-ipv6ready   = true
    landb-mainuser    = var.owner
    landb-responsible = var.owner
    terraform-managed = 1
  })
  name_sanitized     = trimsuffix(var.name, ".cern.ch")
  medium_id          = var.medium_id != null ? var.medium_id : (var.image_centos_stream ? 18 : null)
  operatingsystem_id = var.operatingsystem_id != null ? var.operatingsystem_id : (var.image_centos_stream ? 112 : null)
}
