terraform {
  required_version = ">= 0.14.0"
}

# The image ids are hardcoded until Linux support provides stable names (RQF2017227)
locals {
  CC7_ID = "b5b1f947-8d57-42f6-ad34-7c6478083f46" # CC7 - x86_64 [2022-04-01]
  CS8_ID = "13eaf2da-d5c0-4922-b309-cb054b6e290f" # CS8 - x86_64 [2022-04-01]
  CS9_ID = "3fe03a04-203f-47e2-adc1-d2b06bcadcd7" # CS9 - x86_64 [2022-04-04]
}

provider "foreman" {
  server_hostname       = "foremanlb.cern.ch:8443"
  server_protocol       = "https"
  location_id           = -1
  client_auth_negotiate = true
}

# Simple example
module "cern-puppet" "simple" {
  source      = "git::https://gitlab.cern.ch/ceph/terraform/terraform-module-cern.git/modules/cern-puppet?ref=VERSION_HERE"
  name        = "my-host-simple"
  owner       = "my-group"
  hostgroup   = "my/host/group"
  flavor_name = "m2.medium"
  image_id    = local.CS8_ID
}

# Simple example with random generated suffix
module "cern-puppet" "simple_suffix" {
  source         = "git::https://gitlab.cern.ch/ceph/terraform/terraform-module-cern.git/modules/cern-puppet?ref=VERSION_HERE"
  name           = "my-host-simple-"
  suffix_enabled = true
  owner          = "my-group"
  hostgroup      = "my/host/group"
  flavor_name    = "m2.medium"
  image_id       = local.CS8_ID
}

# Legacy VM created before terraform (with a different image_id)
module "cern-puppet" "legacy" {
  source       = "git::https://gitlab.cern.ch/ceph/terraform/terraform-module-cern.git/modules/cern-puppet?ref=VERSION_HERE"
  name         = "my-host-legacy"
  owner        = "my-group"
  hostgroup    = "my/host/group"
  flavor_name  = "m2.medium"
  image_id     = local.CC7_ID
  image_legacy = true
  # Also specify that we do not use CentOS stream
  image_centos_stream = false
}

# Future VM once Linux support provides stable names (RQF2017227)
module "cern-puppet" "future" {
  source      = "git::https://gitlab.cern.ch/ceph/terraform/terraform-module-cern.git/modules/cern-puppet?ref=VERSION_HERE"
  name        = "my-host-future"
  owner       = "my-group"
  hostgroup   = "my/host/group"
  flavor_name = "m2.medium"
  image_name  = "CS8 - x86_64"
}
