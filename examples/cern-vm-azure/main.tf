#
# Examples to create Puppet managed Azure VMs registered in LandDB
#
# The CERN provider requires the following environment variables to function properly:
# * `CERN_LANDB_USERNAME`
# * `CERN_LANDB_PASSWORD`
#
# Those credentials must be managers of the LanDB cluster used.
#

# Variables for the examples
locals {
  landb_batch = {
    name       = "batch-3rd"
    first_name = "E-GROUP"
    department = "IT"
    group      = "CM"
  }

  landb_manager = {
    name       = "BATCH-XCLOUD-OPERATIONS"
    first_name = "E-GROUP"
    department = "IT"
    group      = "CM"
  }

  landb_cluster_name = "XBATCH-LANDB-AZURE-VM-CLUSTER"
  landb_service_name = "S513-C-VM2"
  landb_address_type = "PUBLIC"
}

#
# Create a single virtual machine
#
module "azure-puppet-vm" "single-machine" {
  source = "git::https://gitlab.cern.ch/ceph/terraform/terraform-module-cern.git//modules/cern-vm-azure?ref=VERSION_HERE"

  resource_group_name = "rg-batch-tests"
  name                = "single-azure-vm"
  ssh_public_key      = ""
  location            = "west-europe"
  username            = "xcloud-admin"
  subnet_id           = subnet_id
  size                = "sadsf"

  hostgroup          = "bi/condor/gridworker/spare"
  puppet_environment = "qa"

  landb_cluster_name = local.landb_cluster_name
  landb_service_name = local.landb_service_name
  landb_address_type = local.landb_address_type
  landb_responsible  = local.landb_batch
  landb_user         = local.landb_batch
  landb_manager      = local.landb_manager
}

#
# Create multiple virtual machines
#
module "azure-puppet-vm" "machine" {
  source = "git::https://gitlab.cern.ch/ceph/terraform/terraform-module-cern.git//modules/cern-vm-azure?ref=VERSION_HERE"

  count = 5

  resource_group_name = "rg-batch-tests"
  name                = "single-azure-vm-${count.index}"
  ssh_public_key      = ""
  location            = "west-europe"
  username            = "xcloud-admin"
  subnet_id           = subnet_id
  size                = "sadsf"

  hostgroup = "bi/condor/gridworker/spare"

  landb_cluster_name = local.landb_cluster_name
  landb_service_name = local.landb_service_name
  landb_address_type = local.landb_address_type
  landb_responsible  = local.landb_batch
  landb_user         = local.landb_batch
  landb_manager      = local.landb_manager
}
