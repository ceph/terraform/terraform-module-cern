#!/bin/sh

set -e

function print_help()
{
    echo "usage: import-foreman-hostgroup.sh [--help] [--dry-run] [--terragrunt]"
    echo "                                   <terraform_resource> <hostgroup_title>"
}

function terraform_cmd()
{
    cmd="terraform"
    if [ "${TERRAGRUNT_ENABLED}" -eq 1 ]; then
        cmd="terragrunt"
    fi
    if [ "${DRY_RUN_ENABLED}" -eq 1 ]; then
        echo "${cmd}" "$@"
        return 0
    fi

    "${cmd}" "$@"
}

options=$(getopt --options h --longoptions help,dry-run,terragrunt -- "$@")
if [ "$?" -ne 0 ]; then
    echo "Incorrect options provided"
    exit 1
fi

DRY_RUN_ENABLED=0
TERRAGRUNT_ENABLED=0

eval set -- "$options"
while true; do
    case "$1" in
    -h|--help)
        print_help
        exit 0
        ;;
    --dry-run)
        DRY_RUN_ENABLED=1
        ;;
    --terragrunt)
        TERRAGRUNT_ENABLED=1
        ;;
    --)
        shift
        break
        ;;
    esac
    shift
done

if [ "$#" -ne 2 ]; then
    print_help
    exit 1
fi

TF_RESOURCE=$1
HOSTGROUP=$2

foreman_id=$(curl \
    --silent --negotiate -u ':' \
    https://foremanlb.cern.ch:8443/api/hostgroups/?search=title%3D${HOSTGROUP} \
    | jq -r '.results[0].id')

([ -z "${foreman_id}" ] || [ "${foreman_id}" = "null" ]) && echo "Can't find hostgroup '${HOSTGROUP}' on foreman" && exit 1
terraform_cmd import "${TF_RESOURCE}" "${foreman_id}"
