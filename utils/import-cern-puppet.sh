#!/bin/sh

set -e

function print_help()
{
    echo "usage: import-cern-puppet.sh [--help] [--suffix-enabled <prefix>] [--legacy]"
    echo "                             [--dry-run] [--terragrunt] [--count <count>]"
    echo "                             <terraform_resource> <hostnames>"
    echo
    echo "EXAMPLE: ./import-cern-puppet.sh module.cern-puppet.simple my-super-host.cern.ch"
    echo "         ./import-cern-puppet.sh --count 2 module.cern-puppet.simple host1.cern.ch host2.cern.ch"
    echo "         ./import-cern-puppet.sh --suffix-enabled myhost- module.cern-puppet.simple myhost-4a93514bf3.cern.ch"
}

function terraform_cmd()
{
    cmd="terraform"
    if [ "${TERRAGRUNT_ENABLED}" -eq 1 ]; then
        cmd="terragrunt"
    fi
    if [ "${DRY_RUN_ENABLED}" -eq 1 ]; then
        echo "${cmd}" "$@"
        return 0
    fi

    "${cmd}" "$@"
}

function import_host()
{
    tf_resource=$1
    host=$2

    os_resource="instance"
    [ "$LEGACY_ENABLED" -eq 1 ] && os_resource="instance_legacy"
    os_id=$(openstack server show "${host}" -f json | jq -r '.id')
    [ -z "$os_id" ] && exit 1

    foreman_id=$(curl \
        --silent --negotiate -u ':' \
        https://foremanlb.cern.ch:8443/api/hosts/?search=name%3D${host}.cern.ch \
        | jq -r '.results[0].id')
    ([ -z "${foreman_id}" ] || [ "${foreman_id}" = "null" ]) && \
        echo "Can't find server '${host}.cern.ch' on foreman" && exit 1

    terraform_cmd import "${tf_resource}.cern_certmgr.cert" "${host}.cern.ch"
    terraform_cmd import "${tf_resource}.cern_roger.state[0]" "${host}.cern.ch"
    terraform_cmd import "${tf_resource}.openstack_compute_instance_v2.${os_resource}[0]" "${os_id}"
    terraform_cmd import "${tf_resource}.foreman_host.host" "${foreman_id}"
}

function import_random()
{
    tf_resource=$1
    host=$2

    suffix="${host#${PREFIX}}"
    [ "${suffix}" = "${host}" ] && \
        echo "invalid host name '${host}' with provided prefix '${PREFIX}'" && \
        exit 1

    terraform_cmd import "${tf_resource}.random_string.suffix[0]" "${host#${PREFIX}}"
}

options=$(getopt --options h --longoptions help,dry-run,terragrunt,legacy,suffix-enabled:,count: -- "$@")
if [ "$?" -ne 0 ]; then
    echo "Incorrect options provided"
    exit 1
fi

COUNT_ENABLED=0
LEGACY_ENABLED=0
DRY_RUN_ENABLED=0
TERRAGRUNT_ENABLED=0
COUNT=1

eval set -- "$options"
while true; do
    case "$1" in
    -h|--help)
        print_help
        exit 0
        ;;
    --legacy)
        LEGACY_ENABLED=1
        ;;
    --dry-run)
        DRY_RUN_ENABLED=1
        ;;
    --terragrunt)
        TERRAGRUNT_ENABLED=1
        ;;
    --suffix-enabled)
        shift
        PREFIX=$1
        ;;
    --count)
        shift
        COUNT=$1
        COUNT_ENABLED=1
        if ! [ "$COUNT" -ge 1 ]; then
            echo "--count options must be a number >= 1"
            exit 1
        fi
        ;;
    --)
        shift
        break
        ;;
    esac
    shift
done

if [ "$#" -eq 0 ]; then
    print_help
    exit 1
fi

TF_RESOURCE=$1
shift

if [ "$COUNT" -ne "$#" ]; then
    echo "Incorrect number of hostname(s) provided: expected '${COUNT}' actual '$#' ($*)"
    exit 1
fi

i=0
for host in "$@"; do
    tf_resource="${TF_RESOURCE}"
    host="${host%.cern.ch}"
    if [ "${COUNT_ENABLED}" -eq 1 ]; then
        tf_resource="${TF_RESOURCE}[$((i++))]"
    fi
    if [ -n "${PREFIX}" ]; then
        import_random "${tf_resource}" "${host}"
    fi
    import_host "${tf_resource}" "${host}"
done
