# terraform-module-cern

Module with CERN-wide infrastructure functionality

- cern-puppet: Create an OpenStack instance managed by puppet on the CERN
  infrastructure
- cern-vm-azure: Create an Azure VM managed by CERN infrastracture (deprecated)

## Import existing instance

To import existing instance for the cern-puppet module we have provided a
script in `utils/import-cern-puppet.sh`. This script is meant to be run on
aiadm (or at least any node that can access OpenStack and Foreman API).

For instance, the following command would allow you to import the first vm in
the example directory:
```sh
./import-cern-puppet.sh cern-puppet.simple my-host-simple
```

This script should allow you to import any variant of arguments of the
cern-puppet module. Please check `./import-cern-puppet.sh --help` for more
information on the various features of this script.

## Development 

* https://www.terraform.io/docs/modules/sources.html#modules-in-package-sub-directories
